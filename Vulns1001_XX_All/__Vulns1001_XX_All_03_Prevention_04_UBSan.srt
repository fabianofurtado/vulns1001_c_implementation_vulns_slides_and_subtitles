1
00:00:00,080 --> 00:00:05,920
undefined behavior sanitizer or ubison

2
00:00:02,960 --> 00:00:08,559
is a mechanism like asan that is enabled

3
00:00:05,920 --> 00:00:10,800
at compile time and then subsequently

4
00:00:08,559 --> 00:00:12,880
performs checks at runtime to look for

5
00:00:10,800 --> 00:00:15,599
programming errors so the difference

6
00:00:12,880 --> 00:00:18,320
between ubison and asen is that whereas

7
00:00:15,599 --> 00:00:21,199
asan focuses on memory access type

8
00:00:18,320 --> 00:00:22,960
errors ubison focuses more on what are

9
00:00:21,199 --> 00:00:25,199
technically undefined behaviors

10
00:00:22,960 --> 00:00:27,279
according to the C specification

11
00:00:25,199 --> 00:00:29,439
now it also does include some things

12
00:00:27,279 --> 00:00:32,000
that are not technically undefined

13
00:00:29,439 --> 00:00:34,719
behaviors but which very often lead to

14
00:00:32,000 --> 00:00:37,120
erroneous problems in code things like

15
00:00:34,719 --> 00:00:40,000
unsigned integer overflow detection

16
00:00:37,120 --> 00:00:42,640
because while unsigned integer overflow

17
00:00:40,000 --> 00:00:45,280
is not technically undefined signed

18
00:00:42,640 --> 00:00:48,079
integer overflow is ubisone can be used

19
00:00:45,280 --> 00:00:50,160
both for prevention and detection in the

20
00:00:48,079 --> 00:00:52,640
sense that when these compile options

21
00:00:50,160 --> 00:00:53,600
are set they will cause you know crash

22
00:00:52,640 --> 00:00:55,760
upon

23
00:00:53,600 --> 00:00:58,160
invocation of some invalid program

24
00:00:55,760 --> 00:01:00,160
behavior so that will prevent the

25
00:00:58,160 --> 00:01:02,000
attacker from taking control of things

26
00:01:00,160 --> 00:01:03,840
because basically you just allowed your

27
00:01:02,000 --> 00:01:05,840
program to crash instead of letting them

28
00:01:03,840 --> 00:01:07,840
take over but it can also be used for

29
00:01:05,840 --> 00:01:10,080
detection in the sense that if you

30
00:01:07,840 --> 00:01:12,159
enable these behaviors as part of your

31
00:01:10,080 --> 00:01:14,960
qa tests and things like that then you

32
00:01:12,159 --> 00:01:16,720
can go ahead and detect them in qa

33
00:01:14,960 --> 00:01:19,360
before actually shipping it into

34
00:01:16,720 --> 00:01:22,560
production with a potential erroneous

35
00:01:19,360 --> 00:01:25,040
issue in the code unlike asan ubison has

36
00:01:22,560 --> 00:01:27,520
a lot less overhead and consequently it

37
00:01:25,040 --> 00:01:29,439
is appropriate to run ubison on

38
00:01:27,520 --> 00:01:31,200
production code so you can enable it in

39
00:01:29,439 --> 00:01:32,799
the compiler with your code that

40
00:01:31,200 --> 00:01:34,640
actually ships out the door

41
00:01:32,799 --> 00:01:37,360
so then it's just a question of which

42
00:01:34,640 --> 00:01:40,159
ubison checks should you enable well

43
00:01:37,360 --> 00:01:42,240
which checks it depends see the website

44
00:01:40,159 --> 00:01:43,759
for this because the yuba-san is going

45
00:01:42,240 --> 00:01:46,560
to have a bunch of different checks

46
00:01:43,759 --> 00:01:48,000
which apply to different vulnerabilities

47
00:01:46,560 --> 00:01:50,799
not all of which we've actually learned

48
00:01:48,000 --> 00:01:50,799
about yet

