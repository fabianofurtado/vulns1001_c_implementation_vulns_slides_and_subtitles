1
00:00:00,480 --> 00:00:05,680
the address-based layout randomization

2
00:00:02,480 --> 00:00:07,440
or ASLR mitigation is the act of

3
00:00:05,680 --> 00:00:09,920
randomizing where things are found in

4
00:00:07,440 --> 00:00:11,679
memory such as the stack the heap or

5
00:00:09,920 --> 00:00:13,360
executable code

6
00:00:11,679 --> 00:00:14,960
so for instance if you imagine that

7
00:00:13,360 --> 00:00:16,400
you've got you know virtual memory is

8
00:00:14,960 --> 00:00:18,480
set up like this and there's some code

9
00:00:16,400 --> 00:00:20,240
in it and some stack in the heap

10
00:00:18,480 --> 00:00:22,640
well the idea is that there's some sort

11
00:00:20,240 --> 00:00:25,119
of re-randomization interval at which

12
00:00:22,640 --> 00:00:27,359
time things are shifted around a memory

13
00:00:25,119 --> 00:00:29,599
so if an attacker made an exploit that

14
00:00:27,359 --> 00:00:31,679
like hard-coded an assumption that you

15
00:00:29,599 --> 00:00:32,960
know some code existed here or some data

16
00:00:31,679 --> 00:00:35,280
existed there

17
00:00:32,960 --> 00:00:37,120
then their assumptions will be invalid

18
00:00:35,280 --> 00:00:39,440
across randomizations

19
00:00:37,120 --> 00:00:41,360
and so this would just keep taking place

20
00:00:39,440 --> 00:00:43,360
and continuously make it more and more

21
00:00:41,360 --> 00:00:45,280
difficult for an attacker the equivalent

22
00:00:43,360 --> 00:00:47,280
thing exists in kernel space for the

23
00:00:45,280 --> 00:00:49,039
actual randomization of the operating

24
00:00:47,280 --> 00:00:51,600
system kernel itself

25
00:00:49,039 --> 00:00:53,680
so ASLR can apply to things like user

26
00:00:51,600 --> 00:00:55,600
space kernel space firmware

27
00:00:53,680 --> 00:00:57,680
virtualization

28
00:00:55,600 --> 00:00:59,280
and so the idea is that you know the

29
00:00:57,680 --> 00:01:01,359
first thing that is responsible for

30
00:00:59,280 --> 00:01:02,960
construction of memory and whether the

31
00:01:01,359 --> 00:01:05,119
memory layout whether that's the layout

32
00:01:02,960 --> 00:01:07,119
of an individual process the layout of

33
00:01:05,119 --> 00:01:09,840
the os kernel layout of the

34
00:01:07,119 --> 00:01:11,920
virtualization system or the firmware

35
00:01:09,840 --> 00:01:14,320
whatever is setting up that initial

36
00:01:11,920 --> 00:01:16,479
memory layout is responsible for having

37
00:01:14,320 --> 00:01:18,880
some option to randomize layout so it's

38
00:01:16,479 --> 00:01:20,080
not just fixed always at hard coded

39
00:01:18,880 --> 00:01:21,759
addresses

40
00:01:20,080 --> 00:01:23,600
and so again this makes it much harder

41
00:01:21,759 --> 00:01:24,880
for an attacker because they would like

42
00:01:23,600 --> 00:01:27,200
to have it be the case that they can

43
00:01:24,880 --> 00:01:28,960
always know that you know some juicy bit

44
00:01:27,200 --> 00:01:30,640
of data like a function pointer can be

45
00:01:28,960 --> 00:01:32,159
found at some particular address and if

46
00:01:30,640 --> 00:01:33,680
they just overwrite that function

47
00:01:32,159 --> 00:01:35,600
pointer at that address then boom

48
00:01:33,680 --> 00:01:38,840
whenever it's called it jumps to attack

49
00:01:35,600 --> 00:01:41,360
controlled code but ASLR makes that

50
00:01:38,840 --> 00:01:43,360
harder when it comes to availability

51
00:01:41,360 --> 00:01:45,280
it's ultimately up to the execution

52
00:01:43,360 --> 00:01:47,759
environment to support it meaning the

53
00:01:45,280 --> 00:01:50,000
operating system vmm or firmware has to

54
00:01:47,759 --> 00:01:52,799
explicitly have this mechanism for

55
00:01:50,000 --> 00:01:54,560
randomization so if you are a operating

56
00:01:52,799 --> 00:01:57,040
system virtualization or firmware

57
00:01:54,560 --> 00:01:59,680
programmer that means it's your job to

58
00:01:57,040 --> 00:02:01,439
actually enable this mechanism and so

59
00:01:59,680 --> 00:02:03,680
while most operating systems have it at

60
00:02:01,439 --> 00:02:05,680
this point it's a little bit less common

61
00:02:03,680 --> 00:02:08,160
or let's say less robust on

62
00:02:05,680 --> 00:02:11,120
virtualization systems and it's almost

63
00:02:08,160 --> 00:02:13,120
non-existent on most firmware systems

64
00:02:11,120 --> 00:02:14,959
so ultimately you know again this is a

65
00:02:13,120 --> 00:02:17,360
important exploit mitigation and that's

66
00:02:14,959 --> 00:02:19,040
why things like firmware tend to be

67
00:02:17,360 --> 00:02:20,640
more vulnerable because they're lacking

68
00:02:19,040 --> 00:02:22,400
this mitigation

69
00:02:20,640 --> 00:02:25,280
additionally if you're you know just a

70
00:02:22,400 --> 00:02:28,640
user space programmer it may require you

71
00:02:25,280 --> 00:02:30,319
opting in at compile time to say yes my

72
00:02:28,640 --> 00:02:32,160
program is compatible with address-based

73
00:02:30,319 --> 00:02:33,599
layout randomization no I don't make any

74
00:02:32,160 --> 00:02:35,599
hard-coded assumptions about where

75
00:02:33,599 --> 00:02:37,440
things are in memory

76
00:02:35,599 --> 00:02:39,680
the reason this is typically required is

77
00:02:37,440 --> 00:02:41,680
because of backwards compatibility and

78
00:02:39,680 --> 00:02:43,519
stuff like that operating systems don't

79
00:02:41,680 --> 00:02:45,120
just necessarily force this on for

80
00:02:43,519 --> 00:02:48,319
everyone although sometimes they have

81
00:02:45,120 --> 00:02:51,360
that option so to make it nicer

82
00:02:48,319 --> 00:02:52,640
typically a application must say yes i

83
00:02:51,360 --> 00:02:54,319
support address-based layout

84
00:02:52,640 --> 00:02:56,000
randomization to your operating system

85
00:02:54,319 --> 00:02:57,680
you can move my code around and it's not

86
00:02:56,000 --> 00:02:59,200
going to break me one bit now when it

87
00:02:57,680 --> 00:03:01,040
comes to the effectiveness of

88
00:02:59,200 --> 00:03:04,239
address-based layout randomization as an

89
00:03:01,040 --> 00:03:07,280
exploit mitigation it sort of depends so

90
00:03:04,239 --> 00:03:09,440
on 32-bit systems there's probably less

91
00:03:07,280 --> 00:03:11,519
address space that's free so again it

92
00:03:09,440 --> 00:03:14,239
just all comes down to 32-bit means

93
00:03:11,519 --> 00:03:15,840
you've got four gigabytes and so is the

94
00:03:14,239 --> 00:03:18,480
system using most of those four

95
00:03:15,840 --> 00:03:20,560
gigabytes if the system is like Windows

96
00:03:18,480 --> 00:03:22,400
or Linux or some large operating system

97
00:03:20,560 --> 00:03:24,319
it's probably using a lot of that space

98
00:03:22,400 --> 00:03:26,480
so the available

99
00:03:24,319 --> 00:03:28,640
memory randomization is less

100
00:03:26,480 --> 00:03:30,879
specifically on Windows 32-bit system it

101
00:03:28,640 --> 00:03:32,879
only has eight bits of wrench entropy

102
00:03:30,879 --> 00:03:35,200
eight bits of randomization that can

103
00:03:32,879 --> 00:03:38,000
occur for the addresses for exes in

104
00:03:35,200 --> 00:03:39,599
memory and 14 bits for dlls or shared

105
00:03:38,000 --> 00:03:40,879
libraries in memory

106
00:03:39,599 --> 00:03:42,560
and so that means you know if there's

107
00:03:40,879 --> 00:03:44,400
only eight bits of memory that means an

108
00:03:42,560 --> 00:03:47,120
attacker you know theoretically has a

109
00:03:44,400 --> 00:03:49,120
one in 256 chance to you know

110
00:03:47,120 --> 00:03:51,040
successfully guess a hard-coded address

111
00:03:49,120 --> 00:03:52,879
where they'd like to find something

112
00:03:51,040 --> 00:03:54,720
so if they're trying to attack something

113
00:03:52,879 --> 00:03:57,120
like a daemon that will just

114
00:03:54,720 --> 00:03:58,879
automatically restart after it's crashed

115
00:03:57,120 --> 00:04:00,720
then they could just keep trying keep

116
00:03:58,879 --> 00:04:02,080
trying keep trying and eventually they

117
00:04:00,720 --> 00:04:03,760
will succeed

118
00:04:02,080 --> 00:04:05,519
when it comes to embedded systems and

119
00:04:03,760 --> 00:04:07,200
firmware type devices

120
00:04:05,519 --> 00:04:08,799
they have the advantage that they're not

121
00:04:07,200 --> 00:04:10,480
necessarily having a giant amount of

122
00:04:08,799 --> 00:04:12,480
code like a full-fledged operating

123
00:04:10,480 --> 00:04:14,000
system on a desktop or server but they

124
00:04:12,480 --> 00:04:15,360
have the disadvantage that they will

125
00:04:14,000 --> 00:04:17,199
typically have

126
00:04:15,360 --> 00:04:19,199
memory mapped I o ranges that are

127
00:04:17,199 --> 00:04:20,479
typically hard coded and fixed and

128
00:04:19,199 --> 00:04:22,720
required

129
00:04:20,479 --> 00:04:24,479
by the actual hardware maker so the

130
00:04:22,720 --> 00:04:26,560
randomization there can be quite a bit

131
00:04:24,479 --> 00:04:28,720
less effective and that's again why i

132
00:04:26,560 --> 00:04:30,320
said that you know firmware very

133
00:04:28,720 --> 00:04:32,720
uncommon for address-based layout

134
00:04:30,320 --> 00:04:34,800
randomization to exist but there's no

135
00:04:32,720 --> 00:04:36,880
reason that it can't then when it comes

136
00:04:34,800 --> 00:04:38,800
to full 64-bit systems well there's a

137
00:04:36,880 --> 00:04:41,199
whole lot more address space that's not

138
00:04:38,800 --> 00:04:43,040
being used and consequently you can have

139
00:04:41,199 --> 00:04:44,960
more effective randomization other

140
00:04:43,040 --> 00:04:47,840
design decisions that can affect the

141
00:04:44,960 --> 00:04:50,160
effectiveness of ASLR are things like

142
00:04:47,840 --> 00:04:52,960
what is the re-randomization interval

143
00:04:50,160 --> 00:04:55,199
so for instance you know if the system

144
00:04:52,960 --> 00:04:56,720
is only re-randomized at every reboot

145
00:04:55,199 --> 00:04:58,720
and people are keeping their machines

146
00:04:56,720 --> 00:05:00,880
without rebooting for weeks or months at

147
00:04:58,720 --> 00:05:02,639
a time well then you know perhaps

148
00:05:00,880 --> 00:05:05,039
there's not as much effectiveness to

149
00:05:02,639 --> 00:05:06,800
that uh on the other hand it's extremely

150
00:05:05,039 --> 00:05:09,440
difficult to just sort of like jumble

151
00:05:06,800 --> 00:05:11,520
everything up all you know at a moment's

152
00:05:09,440 --> 00:05:13,039
notice without a whole bunch of stuff

153
00:05:11,520 --> 00:05:14,880
breaking on the system

154
00:05:13,039 --> 00:05:17,120
so typically you can have things like

155
00:05:14,880 --> 00:05:18,800
perhaps at every process launch the

156
00:05:17,120 --> 00:05:20,320
process could end up at a different

157
00:05:18,800 --> 00:05:22,880
memory address because it had been torn

158
00:05:20,320 --> 00:05:24,639
down and rebuilt but you know again

159
00:05:22,880 --> 00:05:26,560
something like a firmware that's just

160
00:05:24,639 --> 00:05:28,720
continuously running a big monolithic

161
00:05:26,560 --> 00:05:30,560
blob forever that's hard to re-randomize

162
00:05:28,720 --> 00:05:32,560
the same for operating systems and

163
00:05:30,560 --> 00:05:34,720
virtualization systems

164
00:05:32,560 --> 00:05:36,400
when it comes to again user space type

165
00:05:34,720 --> 00:05:38,000
things you might have

166
00:05:36,400 --> 00:05:40,479
design decisions that were made by an

167
00:05:38,000 --> 00:05:42,400
operating system to for instance map

168
00:05:40,479 --> 00:05:44,160
shared libraries things like dlls shared

169
00:05:42,400 --> 00:05:46,000
libraries on Linux

170
00:05:44,160 --> 00:05:48,000
you might have design decisions to map

171
00:05:46,000 --> 00:05:51,120
them at the same virtual address space

172
00:05:48,000 --> 00:05:53,280
across all the different processes for

173
00:05:51,120 --> 00:05:55,440
optimization purposes and copy on write

174
00:05:53,280 --> 00:05:58,400
and memory sharing and stuff like that

175
00:05:55,440 --> 00:06:00,160
and that can mean that for instance if a

176
00:05:58,400 --> 00:06:02,000
memory address is disclosed in one

177
00:06:00,160 --> 00:06:04,080
process it could potentially be reused

178
00:06:02,000 --> 00:06:06,240
in another process if they're trying to

179
00:06:04,080 --> 00:06:07,520
for instance find you know some code

180
00:06:06,240 --> 00:06:08,560
that they want to reuse in a given

181
00:06:07,520 --> 00:06:11,199
library

182
00:06:08,560 --> 00:06:13,199
now when it comes to not just looking at

183
00:06:11,199 --> 00:06:15,039
weaknesses of the implementation but

184
00:06:13,199 --> 00:06:17,919
instead just architecturally trying to

185
00:06:15,039 --> 00:06:20,560
bypass an exploit mitigation like ASLR

186
00:06:17,919 --> 00:06:22,160
an attacker can do this by utilizing a

187
00:06:20,560 --> 00:06:23,840
different class of vulnerability that we

188
00:06:22,160 --> 00:06:26,479
haven't learned about yet called

189
00:06:23,840 --> 00:06:28,720
information disclosure or info leaks and

190
00:06:26,479 --> 00:06:31,280
the basic idea there is that ASLR's

191
00:06:28,720 --> 00:06:32,960
strength is based on the idea of an

192
00:06:31,280 --> 00:06:34,639
attacker can't know where something

193
00:06:32,960 --> 00:06:36,080
that's been randomized is it's assumed

194
00:06:34,639 --> 00:06:37,680
that you know if you're in a 64-bit

195
00:06:36,080 --> 00:06:40,080
address space there's just too many

196
00:06:37,680 --> 00:06:41,759
choices they can't uh they're not going

197
00:06:40,080 --> 00:06:44,000
to have a high enough probability to

198
00:06:41,759 --> 00:06:46,000
successfully guess so what are they

199
00:06:44,000 --> 00:06:47,680
going to do they're going to not guess

200
00:06:46,000 --> 00:06:50,080
they're going to try to find something

201
00:06:47,680 --> 00:06:52,240
that allows them to read memory and then

202
00:06:50,080 --> 00:06:53,840
based on the reading of information they

203
00:06:52,240 --> 00:06:56,639
can potentially figure out you know how

204
00:06:53,840 --> 00:06:58,319
much the memory was slid by and figure

205
00:06:56,639 --> 00:07:00,000
out what that displacement is and

206
00:06:58,319 --> 00:07:01,840
consequently understand okay well if

207
00:07:00,000 --> 00:07:03,360
everything's been moved this much then

208
00:07:01,840 --> 00:07:05,440
the thing that I'm actually looking for

209
00:07:03,360 --> 00:07:08,080
is this much plus x

210
00:07:05,440 --> 00:07:09,120
so info leaks are the type of thing that

211
00:07:08,080 --> 00:07:11,440
just allow an attacker to

212
00:07:09,120 --> 00:07:13,360
architecturally bypass it now it still

213
00:07:11,440 --> 00:07:14,880
is beneficial to have the mitigation in

214
00:07:13,360 --> 00:07:16,880
place because you're effectively making

215
00:07:14,880 --> 00:07:19,120
the attacker find an extra bug instead

216
00:07:16,880 --> 00:07:21,360
of you know one bug to win now they've

217
00:07:19,120 --> 00:07:24,400
got to find two bugs and indeed these

218
00:07:21,360 --> 00:07:27,680
are a fundamental staple of

219
00:07:24,400 --> 00:07:29,280
exploit chains that try to bypass

220
00:07:27,680 --> 00:07:32,479
that try to successfully exploit the

221
00:07:29,280 --> 00:07:34,960
system on systems that support eslr as

222
00:07:32,479 --> 00:07:37,840
mentioned previously compilation time

223
00:07:34,960 --> 00:07:40,160
opt-in is often required so check the

224
00:07:37,840 --> 00:07:42,080
website and we will provide the compiler

225
00:07:40,160 --> 00:07:44,080
options that are necessary for you to

226
00:07:42,080 --> 00:07:47,199
say my program supports address based

227
00:07:44,080 --> 00:07:47,199
layout randomization

