1
00:00:00,240 --> 00:00:04,560
further along the line of reasoning that

2
00:00:02,080 --> 00:00:07,200
it is difficult to read all the code to

3
00:00:04,560 --> 00:00:08,480
find all the bugs is the use of dynamic

4
00:00:07,200 --> 00:00:10,240
analysis

5
00:00:08,480 --> 00:00:12,160
dynamic analysis is when you actually

6
00:00:10,240 --> 00:00:14,320
run the code instead of just static

7
00:00:12,160 --> 00:00:16,080
analysis which is when you read it

8
00:00:14,320 --> 00:00:18,640
and one of the most common and fruitful

9
00:00:16,080 --> 00:00:21,520
techniques is to utilize a fuzzer

10
00:00:18,640 --> 00:00:24,240
fuzzing is the act of feeding randomized

11
00:00:21,520 --> 00:00:27,199
ACID into the attack surface of a

12
00:00:24,240 --> 00:00:28,720
program so the fuzzer is the thing that

13
00:00:27,199 --> 00:00:30,640
is doing the fuzzing it is an

14
00:00:28,720 --> 00:00:33,120
application or a test harness that feeds

15
00:00:30,640 --> 00:00:36,000
the random data into the program

16
00:00:33,120 --> 00:00:38,640
now it very often finds bugs very

17
00:00:36,000 --> 00:00:40,160
quickly if it is an unhardened program

18
00:00:38,640 --> 00:00:42,399
where people have not programmed

19
00:00:40,160 --> 00:00:44,800
paranoid and therefore it is actually a

20
00:00:42,399 --> 00:00:46,800
favored technique of real attackers a

21
00:00:44,800 --> 00:00:49,039
defender's goal when fuzzing is to beat

22
00:00:46,800 --> 00:00:50,559
the attacker at their own game they know

23
00:00:49,039 --> 00:00:52,879
that the attacker is going to feed

24
00:00:50,559 --> 00:00:55,360
randomized garbage input to their attack

25
00:00:52,879 --> 00:00:57,840
surface and therefore they're going to

26
00:00:55,360 --> 00:00:59,680
feed themselves randomized garbage input

27
00:00:57,840 --> 00:01:01,680
to their attack surfaces before the

28
00:00:59,680 --> 00:01:03,600
attacker ever came so this provides the

29
00:01:01,680 --> 00:01:05,280
defender with an advantage in the sense

30
00:01:03,600 --> 00:01:07,680
that they can do this sort of testing

31
00:01:05,280 --> 00:01:09,520
before the software is ever released and

32
00:01:07,680 --> 00:01:11,439
close down bugs before they ever make

33
00:01:09,520 --> 00:01:13,600
their way into the wild on the other

34
00:01:11,439 --> 00:01:15,840
hand attackers might have an advantage

35
00:01:13,600 --> 00:01:18,400
if they're willing to put more time and

36
00:01:15,840 --> 00:01:20,159
effort and cpu cycles and

37
00:01:18,400 --> 00:01:21,680
sophistication and everything else

38
00:01:20,159 --> 00:01:23,920
they're willing to put more effort into

39
00:01:21,680 --> 00:01:25,600
the fuzzing than they have the advantage

40
00:01:23,920 --> 00:01:27,600
so there's a variety of different forms

41
00:01:25,600 --> 00:01:30,400
of fuzzing and we'll start with thumb

42
00:01:27,600 --> 00:01:32,560
fuzzing which is just truly random data

43
00:01:30,400 --> 00:01:34,560
so if we imagine for instance that you

44
00:01:32,560 --> 00:01:36,799
know the attacker wanted to compromise

45
00:01:34,560 --> 00:01:38,799
some software that was parsing tcp

46
00:01:36,799 --> 00:01:40,799
packets then they might know that you

47
00:01:38,799 --> 00:01:42,720
know yes there's some byte structure for

48
00:01:40,799 --> 00:01:45,520
everything but I'm just going to feed a

49
00:01:42,720 --> 00:01:47,040
bunch of randomized data into that so

50
00:01:45,520 --> 00:01:49,439
they don't even care what the structure

51
00:01:47,040 --> 00:01:52,079
is for TCP they just feed random garbage

52
00:01:49,439 --> 00:01:53,520
in and quite frankly quite often that

53
00:01:52,079 --> 00:01:55,520
will be enough to cause a crash which

54
00:01:53,520 --> 00:01:57,600
they can then go analyze to see if it's

55
00:01:55,520 --> 00:01:59,680
anything about this ACID that's being

56
00:01:57,600 --> 00:02:01,200
consumed that leads to a security

57
00:01:59,680 --> 00:02:02,719
vulnerability

58
00:02:01,200 --> 00:02:04,960
now the next step would be something

59
00:02:02,719 --> 00:02:06,960
like template-based fuzzing where they

60
00:02:04,960 --> 00:02:09,280
know that it's a TCP packet that's being

61
00:02:06,960 --> 00:02:11,760
parsed and they can know something about

62
00:02:09,280 --> 00:02:14,319
the structure of the data fields within

63
00:02:11,760 --> 00:02:17,120
that packet so they create a template

64
00:02:14,319 --> 00:02:19,599
based on the anticipated data structures

65
00:02:17,120 --> 00:02:23,200
and then they feed in randomized data

66
00:02:19,599 --> 00:02:25,120
following the template into the code now

67
00:02:23,200 --> 00:02:26,800
if it was just this that would not

68
00:02:25,120 --> 00:02:28,879
really be that much different from dumb

69
00:02:26,800 --> 00:02:30,879
fuzzing right that still literally just

70
00:02:28,879 --> 00:02:32,959
is a bunch of random data that'll be

71
00:02:30,879 --> 00:02:35,440
parsed however it's parsed the benefit

72
00:02:32,959 --> 00:02:36,959
of template-based fuzzing is that

73
00:02:35,440 --> 00:02:39,360
there may be code paths that are

74
00:02:36,959 --> 00:02:41,519
unreachable with you know random

75
00:02:39,360 --> 00:02:43,760
completely invalid inputs you know

76
00:02:41,519 --> 00:02:45,440
unreachable or reachable with extremely

77
00:02:43,760 --> 00:02:47,280
low probability because you know if

78
00:02:45,440 --> 00:02:49,200
you're iterating through you know four

79
00:02:47,280 --> 00:02:51,360
billion possible numbers and if maybe

80
00:02:49,200 --> 00:02:52,800
only like two of them are valid well

81
00:02:51,360 --> 00:02:54,000
then you know most of the time you're

82
00:02:52,800 --> 00:02:56,400
not going to hit the code that you want

83
00:02:54,000 --> 00:02:58,959
to hit so template-based fuzzing allows

84
00:02:56,400 --> 00:03:01,840
people to you know selectively take

85
00:02:58,959 --> 00:03:03,440
areas and make them into sane data so

86
00:03:01,840 --> 00:03:05,040
that you know the data will be parsed

87
00:03:03,440 --> 00:03:07,599
more effectively then there's the

88
00:03:05,040 --> 00:03:10,159
technique called coverage GUIDed fuzzing

89
00:03:07,599 --> 00:03:13,519
which basically creates a feedback loop

90
00:03:10,159 --> 00:03:16,080
within the fuzzer that prioritizes code

91
00:03:13,519 --> 00:03:18,480
going down control flows that have not

92
00:03:16,080 --> 00:03:20,640
been seen before so essentially they say

93
00:03:18,480 --> 00:03:23,040
I'm going to feed random garbage in but

94
00:03:20,640 --> 00:03:24,799
whenever I see that that garbage causes

95
00:03:23,040 --> 00:03:26,799
it to take a different flow in the path

96
00:03:24,799 --> 00:03:27,920
I'm going to like save that and reuse

97
00:03:26,799 --> 00:03:30,000
that later

98
00:03:27,920 --> 00:03:31,920
so I like this particular picture this

99
00:03:30,000 --> 00:03:34,159
extremely nice animation this is a

100
00:03:31,920 --> 00:03:36,480
control flow graph of some particular

101
00:03:34,159 --> 00:03:38,879
code and reverse engineers frequently

102
00:03:36,480 --> 00:03:41,040
look at code in its control flow graph

103
00:03:38,879 --> 00:03:42,959
form where basically you know some node

104
00:03:41,040 --> 00:03:44,640
calls to another node calls to another

105
00:03:42,959 --> 00:03:46,080
node and then there's a conditional here

106
00:03:44,640 --> 00:03:48,159
maybe sometimes it goes this way and

107
00:03:46,080 --> 00:03:50,640
sometimes it goes that way but anyways

108
00:03:48,159 --> 00:03:53,519
this is just animating the notion of for

109
00:03:50,640 --> 00:03:55,840
a given fuzzer when these nodes light up

110
00:03:53,519 --> 00:03:58,319
that means that a given input has caused

111
00:03:55,840 --> 00:03:59,200
the control flow to flow down a given

112
00:03:58,319 --> 00:04:01,360
path

113
00:03:59,200 --> 00:04:03,040
and if you just like pick any given leaf

114
00:04:01,360 --> 00:04:05,439
node on this thing like let's pick this

115
00:04:03,040 --> 00:04:08,159
one right here you know eventually that

116
00:04:05,439 --> 00:04:09,840
does get hit with the fuzzer so coverage

117
00:04:08,159 --> 00:04:11,439
GUIDed fuzzer is trying to say like

118
00:04:09,840 --> 00:04:13,840
let's try to make sure that we get

119
00:04:11,439 --> 00:04:16,079
through all of the coverage like let's

120
00:04:13,840 --> 00:04:18,560
maximize the coverage of the randomized

121
00:04:16,079 --> 00:04:20,000
inputs because the more coverage you

122
00:04:18,560 --> 00:04:21,840
have the more likely you are to find

123
00:04:20,000 --> 00:04:23,440
vulnerabilities right if you only ever

124
00:04:21,840 --> 00:04:25,120
you know hit a small portion of the

125
00:04:23,440 --> 00:04:26,560
control flow graph there could be all

126
00:04:25,120 --> 00:04:28,639
sorts of vulnerabilities over here but

127
00:04:26,560 --> 00:04:30,560
your fuzzer never finds them so that's

128
00:04:28,639 --> 00:04:32,720
why coverage GUIDed fuzzing is extremely

129
00:04:30,560 --> 00:04:34,320
beneficial as well

130
00:04:32,720 --> 00:04:36,479
so the nice thing about fuzzing in

131
00:04:34,320 --> 00:04:38,560
general if you're a defender or even if

132
00:04:36,479 --> 00:04:40,080
you're an attacker is that you know you

133
00:04:38,560 --> 00:04:42,800
don't have to bite off more than you can

134
00:04:40,080 --> 00:04:44,320
chew you get to work in bite size pieces

135
00:04:42,800 --> 00:04:46,880
so if you imagine that there's some

136
00:04:44,320 --> 00:04:50,160
application my cool image viewer could

137
00:04:46,880 --> 00:04:52,639
be Adobe acrobat could be Apple preview

138
00:04:50,160 --> 00:04:54,880
could be Microsoft word there's some

139
00:04:52,639 --> 00:04:57,280
application and that application may

140
00:04:54,880 --> 00:05:00,000
take in and accept and parse and process

141
00:04:57,280 --> 00:05:02,080
and render a bunch of different formats

142
00:05:00,000 --> 00:05:04,560
for instance in this case image formats

143
00:05:02,080 --> 00:05:06,720
but it could be audio it could be fonts

144
00:05:04,560 --> 00:05:09,120
could be video etc

145
00:05:06,720 --> 00:05:10,639
so if a thing supports a bunch of

146
00:05:09,120 --> 00:05:12,880
different formats

147
00:05:10,639 --> 00:05:15,360
then you know in reality each of those

148
00:05:12,880 --> 00:05:17,840
formats is just its own unique attack

149
00:05:15,360 --> 00:05:20,960
surface this is a place that an attacker

150
00:05:17,840 --> 00:05:22,720
can feed you a png or a jpeg or a tiff

151
00:05:20,960 --> 00:05:24,720
and each of those could have their own

152
00:05:22,720 --> 00:05:27,360
unique vulnerabilities

153
00:05:24,720 --> 00:05:29,919
so realistically each of these is an

154
00:05:27,360 --> 00:05:32,479
attack surface unto itself and therefore

155
00:05:29,919 --> 00:05:35,520
a defender or an attacker can pluck out

156
00:05:32,479 --> 00:05:37,680
one of those attack surfaces and fuzz

157
00:05:35,520 --> 00:05:39,120
only that right in the context of an

158
00:05:37,680 --> 00:05:41,440
attacker would mean just creating a

159
00:05:39,120 --> 00:05:44,240
bunch of jpegs and feeding it into the

160
00:05:41,440 --> 00:05:46,400
program but in the context of a defender

161
00:05:44,240 --> 00:05:48,720
if you're the person responsible for

162
00:05:46,400 --> 00:05:49,840
managing this code you can take that

163
00:05:48,720 --> 00:05:51,759
jpeg

164
00:05:49,840 --> 00:05:53,840
code within your code base maybe it's

165
00:05:51,759 --> 00:05:56,479
library maybe it's not and you can pull

166
00:05:53,840 --> 00:05:58,800
it out into some other project which is

167
00:05:56,479 --> 00:06:00,479
capable of you know just wrapping and

168
00:05:58,800 --> 00:06:02,560
throwing a harness around that that is

169
00:06:00,479 --> 00:06:05,199
responsible for creating the randomized

170
00:06:02,560 --> 00:06:07,840
input and then feeding it into the jpeg

171
00:06:05,199 --> 00:06:09,440
maybe it has a template maybe it doesn't

172
00:06:07,840 --> 00:06:11,360
to start with you don't need a template

173
00:06:09,440 --> 00:06:13,520
right you just start as simple and dumb

174
00:06:11,360 --> 00:06:15,280
as you want and again if you haven't

175
00:06:13,520 --> 00:06:16,880
been programming paranoid even a dumb

176
00:06:15,280 --> 00:06:20,720
fuzzer will almost certainly find

177
00:06:16,880 --> 00:06:22,319
crashes due to the consumption of ACID

178
00:06:20,720 --> 00:06:24,960
so basically that's going to be the

179
00:06:22,319 --> 00:06:27,360
situation where you know this is an easy

180
00:06:24,960 --> 00:06:30,160
way for you as a defender or an attacker

181
00:06:27,360 --> 00:06:31,680
to find vulnerabilities and things

182
00:06:30,160 --> 00:06:34,319
now there are of course drawbacks to

183
00:06:31,680 --> 00:06:36,160
fuzzing one of the biggest ones is the

184
00:06:34,319 --> 00:06:37,759
fact that what you get out of fuzzing is

185
00:06:36,160 --> 00:06:39,520
a whole bunch of inputs that cause a

186
00:06:37,759 --> 00:06:41,199
whole bunch of crashes

187
00:06:39,520 --> 00:06:43,360
and consequently you need to do things

188
00:06:41,199 --> 00:06:45,360
like de-duplicating the crashes so a

189
00:06:43,360 --> 00:06:47,120
bunch of different random values could

190
00:06:45,360 --> 00:06:49,199
all be creating the exact same crash

191
00:06:47,120 --> 00:06:51,840
maybe your code crashes anytime a value

192
00:06:49,199 --> 00:06:54,160
is greater than you know half of 2 to

193
00:06:51,840 --> 00:06:56,400
the 32 so that means you know whenever

194
00:06:54,160 --> 00:06:58,960
the random value is picked on you know

195
00:06:56,400 --> 00:07:01,440
greater than 2 to the 31 then it crashes

196
00:06:58,960 --> 00:07:03,840
and less than 2 to 31 it doesn't crash

197
00:07:01,440 --> 00:07:06,000
so deduplicating crashes and figuring

198
00:07:03,840 --> 00:07:07,599
out what is the minimum input necessary

199
00:07:06,000 --> 00:07:10,160
to cause the crash

200
00:07:07,599 --> 00:07:12,800
is important as well because you know if

201
00:07:10,160 --> 00:07:14,880
a analyst if the code author wants to go

202
00:07:12,800 --> 00:07:16,400
find what the cause of this crash is

203
00:07:14,880 --> 00:07:18,080
they'd like something nice and simple

204
00:07:16,400 --> 00:07:19,360
that they can read and then they'd like

205
00:07:18,080 --> 00:07:21,520
to be able to you know step through the

206
00:07:19,360 --> 00:07:23,199
debugger with that very simple input so

207
00:07:21,520 --> 00:07:24,960
that it's very clear like what the root

208
00:07:23,199 --> 00:07:26,479
cause of that is

209
00:07:24,960 --> 00:07:28,960
so you know doing the efficient

210
00:07:26,479 --> 00:07:31,280
deduplication and minimization is

211
00:07:28,960 --> 00:07:32,880
somewhat of a specialty unto itself so

212
00:07:31,280 --> 00:07:34,880
this was just the quickest of skins

213
00:07:32,880 --> 00:07:37,280
through fuzzing the reality is that this

214
00:07:34,880 --> 00:07:39,120
does deserve a class of its own and

215
00:07:37,280 --> 00:07:41,440
hopefully we'll have classes for you to

216
00:07:39,120 --> 00:07:43,280
dive a lot more deeply into this content

217
00:07:41,440 --> 00:07:45,680
in the future to help you build your own

218
00:07:43,280 --> 00:07:45,680
fuzzers

