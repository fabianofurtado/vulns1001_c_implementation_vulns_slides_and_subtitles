1
00:00:00,320 --> 00:00:03,120
now when it comes to preventing the

2
00:00:01,920 --> 00:00:04,799
types of vulnerabilities that we're

3
00:00:03,120 --> 00:00:07,440
going to cover in this class I would be

4
00:00:04,799 --> 00:00:09,519
remiss if I didn't at least mention the

5
00:00:07,440 --> 00:00:11,440
idea of adopting a completely different

6
00:00:09,519 --> 00:00:14,080
language as you'll see through this

7
00:00:11,440 --> 00:00:15,519
entire class these are going to be a lot

8
00:00:14,080 --> 00:00:17,680
of different vulnerabilities that happen

9
00:00:15,519 --> 00:00:20,080
a lot of different places and you know

10
00:00:17,680 --> 00:00:22,640
sometimes it's a lot of work in order to

11
00:00:20,080 --> 00:00:24,400
actually try to combat them and so maybe

12
00:00:22,640 --> 00:00:27,439
it's going to be less work in order to

13
00:00:24,400 --> 00:00:30,560
adopt a different language now

14
00:00:27,439 --> 00:00:32,480
I am not a crazy person so I know that

15
00:00:30,560 --> 00:00:35,360
this is not going to be easy and that's

16
00:00:32,480 --> 00:00:37,680
why you know just by way of trying to

17
00:00:35,360 --> 00:00:38,879
say you know in a reasonable fashion i

18
00:00:37,680 --> 00:00:40,879
know you're not going to be able to

19
00:00:38,879 --> 00:00:42,640
rewrite you know your millions of lines

20
00:00:40,879 --> 00:00:44,879
of source code that you know some of you

21
00:00:42,640 --> 00:00:47,280
have and consequently it's not about you

22
00:00:44,879 --> 00:00:48,960
know rewriting your entire code in a new

23
00:00:47,280 --> 00:00:51,360
more safe language

24
00:00:48,960 --> 00:00:54,320
it's about sort of finding a way to

25
00:00:51,360 --> 00:00:57,120
adopt piecemeal uh the use of languages

26
00:00:54,320 --> 00:00:59,120
like roston go and so forth for just one

27
00:00:57,120 --> 00:01:00,960
component at a time and you know in the

28
00:00:59,120 --> 00:01:03,120
context of user space that might mean

29
00:01:00,960 --> 00:01:04,640
one library at a time it might mean you

30
00:01:03,120 --> 00:01:06,400
know in the context of an operating

31
00:01:04,640 --> 00:01:08,880
system one particular application or

32
00:01:06,400 --> 00:01:10,560
daemon at a time web browsers are

33
00:01:08,880 --> 00:01:12,640
frequently broken up to more and more

34
00:01:10,560 --> 00:01:14,240
pieces these days so get one piece of

35
00:01:12,640 --> 00:01:16,560
the browser at a time one kernel driver

36
00:01:14,240 --> 00:01:19,040
at a time one firmware module at a time

37
00:01:16,560 --> 00:01:21,920
now again because I am not a crazy

38
00:01:19,040 --> 00:01:23,840
person I know that logistics rules

39
00:01:21,920 --> 00:01:26,320
everything around me which means it's

40
00:01:23,840 --> 00:01:28,479
not just a simple matter of you know

41
00:01:26,320 --> 00:01:30,479
adopting one particular dll or kernel

42
00:01:28,479 --> 00:01:32,000
module firmware module I know that

43
00:01:30,479 --> 00:01:33,439
there's a whole bunch of other things

44
00:01:32,000 --> 00:01:35,680
that need consideration to actually

45
00:01:33,439 --> 00:01:37,600
write production code you have to train

46
00:01:35,680 --> 00:01:39,520
all the developers on the new language

47
00:01:37,600 --> 00:01:41,920
you have to integrate with the existing

48
00:01:39,520 --> 00:01:43,840
code you have to find ways to learn how

49
00:01:41,920 --> 00:01:46,079
to debug that code as effectively as you

50
00:01:43,840 --> 00:01:48,240
used to on your C code

51
00:01:46,079 --> 00:01:50,159
finding versus writing libraries to do

52
00:01:48,240 --> 00:01:51,759
what you're trying to do

53
00:01:50,159 --> 00:01:53,439
trying to build the production code for

54
00:01:51,759 --> 00:01:55,759
deployment you know I'm aware that

55
00:01:53,439 --> 00:01:58,640
there's entire build systems that have

56
00:01:55,759 --> 00:02:01,200
to be modified in order to

57
00:01:58,640 --> 00:02:03,520
even create the binaries to you know

58
00:02:01,200 --> 00:02:05,920
incorporate into some overall larger

59
00:02:03,520 --> 00:02:07,360
component and packaging system

60
00:02:05,920 --> 00:02:09,759
there's the things like qa and

61
00:02:07,360 --> 00:02:11,599
regression testing in-field diagnostics

62
00:02:09,759 --> 00:02:12,959
factory deployment if applicable for

63
00:02:11,599 --> 00:02:15,599
things like you know firmware for

64
00:02:12,959 --> 00:02:17,599
instance so you know I have worked at a

65
00:02:15,599 --> 00:02:20,080
company that does production code at

66
00:02:17,599 --> 00:02:23,360
scale I have seen the nitty-gritty ins

67
00:02:20,080 --> 00:02:25,520
and outs of things like you know Apple's

68
00:02:23,360 --> 00:02:27,920
build system and I've learned much more

69
00:02:25,520 --> 00:02:30,800
than I ever wanted to learn about them

70
00:02:27,920 --> 00:02:32,879
so I know it's difficult but defending

71
00:02:30,800 --> 00:02:35,360
all of your C source code is difficult

72
00:02:32,879 --> 00:02:37,200
as well as you'll see in this class so

73
00:02:35,360 --> 00:02:40,239
just something to think about I'm just

74
00:02:37,200 --> 00:02:40,239
putting it out there

